EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:L L2
U 1 1 5CA755DF
P 1550 1050
F 0 "L2" V 1740 1050 50  0000 C CNN
F 1 "10mH" V 1649 1050 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L30.0mm_D8.0mm_P35.56mm_Horizontal_Fastron_77A" H 1550 1050 50  0001 C CNN
F 3 "~" H 1550 1050 50  0001 C CNN
	1    1550 1050
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L3
U 1 1 5CA768FD
P 1150 1050
F 0 "L3" V 1340 1050 50  0000 C CNN
F 1 "10mH" V 1249 1050 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L30.0mm_D8.0mm_P35.56mm_Horizontal_Fastron_77A" H 1150 1050 50  0001 C CNN
F 3 "~" H 1150 1050 50  0001 C CNN
	1    1150 1050
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L4
U 1 1 5CA76EEB
P 900 1300
F 0 "L4" H 952 1346 50  0000 L CNN
F 1 "10mH" H 952 1255 50  0000 L CNN
F 2 "Inductor_THT:L_Axial_L30.0mm_D8.0mm_P35.56mm_Horizontal_Fastron_77A" H 900 1300 50  0001 C CNN
F 3 "~" H 900 1300 50  0001 C CNN
	1    900  1300
	1    0    0    -1  
$EndComp
$Comp
L Device:Antenna AE1
U 1 1 5CA799B4
P 2200 750
F 0 "AE1" H 2280 739 50  0000 L CNN
F 1 "Pitch antenna" H 2280 648 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D4.0mm_Drill2.0mm" H 2200 750 50  0001 C CNN
F 3 "~" H 2200 750 50  0001 C CNN
	1    2200 750 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5CA7A217
P 1200 1900
F 0 "C1" H 1292 1946 50  0000 L CNN
F 1 "3n3" H 1292 1855 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 1200 1900 50  0001 C CNN
F 3 "~" H 1200 1900 50  0001 C CNN
	1    1200 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5CA7B0F8
P 1200 2900
F 0 "R1" H 1259 2946 50  0000 L CNN
F 1 "1K" H 1259 2855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 1200 2900 50  0001 C CNN
F 3 "~" H 1200 2900 50  0001 C CNN
	1    1200 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:LTRIM L5
U 1 1 5CA7C153
P 1550 1700
F 0 "L5" H 1653 1746 50  0000 L CNN
F 1 "47uH" H 1653 1655 50  0000 L CNN
F 2 "Theremin:Toko-1" H 1550 1700 50  0001 C CNN
F 3 "~" H 1550 1700 50  0001 C CNN
	1    1550 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C4
U 1 1 5CA7D497
P 2650 2700
F 0 "C4" H 2738 2746 50  0000 L CNN
F 1 "1uF" H 2738 2655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 2650 2700 50  0001 C CNN
F 3 "~" H 2650 2700 50  0001 C CNN
	1    2650 2700
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N3904 Q1
U 1 1 5CA7DE2A
P 1450 2700
F 0 "Q1" H 1600 2800 50  0000 L CNN
F 1 "2N3904" H 1650 2650 50  0000 L CNN
F 2 "Theremin:TO92-321" H 1650 2625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 1450 2700 50  0001 L CNN
	1    1450 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:L L12
U 1 1 5CA7F474
P 1550 2100
F 0 "L12" H 1603 2146 50  0000 L CNN
F 1 "47uH" H 1603 2055 50  0000 L CNN
F 2 "Inductor_THT:L_Axial_L9.5mm_D4.0mm_P15.24mm_Horizontal_Fastron_SMCC" H 1550 2100 50  0001 C CNN
F 3 "~" H 1550 2100 50  0001 C CNN
	1    1550 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5CA80A43
P 900 2850
F 0 "C2" H 950 2950 50  0000 L CNN
F 1 "15pF" H 950 2750 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 900 2850 50  0001 C CNN
F 3 "~" H 900 2850 50  0001 C CNN
	1    900  2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 2250 1550 2300
Wire Wire Line
	1550 2300 1200 2300
Wire Wire Line
	900  2300 900  1450
Connection ~ 1550 2300
Wire Wire Line
	1550 2300 1550 2500
Wire Wire Line
	1200 2000 1200 2300
Connection ~ 1200 2300
Wire Wire Line
	1200 2300 900  2300
Wire Wire Line
	1550 1850 1550 1950
Wire Wire Line
	1200 1800 1200 1500
Wire Wire Line
	1200 1500 1550 1500
Wire Wire Line
	1550 1500 1550 1550
Wire Wire Line
	900  2750 900  2300
Connection ~ 900  2300
Wire Wire Line
	1250 2700 1200 2700
Wire Wire Line
	1200 2700 1200 2800
$Comp
L power:GND #PWR0101
U 1 1 5CA82E0F
P 1200 3300
F 0 "#PWR0101" H 1200 3050 50  0001 C CNN
F 1 "GND" H 1205 3127 50  0000 C CNN
F 2 "" H 1200 3300 50  0001 C CNN
F 3 "" H 1200 3300 50  0001 C CNN
	1    1200 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 3000 1200 3300
$Comp
L Transistor_BJT:2N3904 Q2
U 1 1 5CA8472C
P 2150 2700
F 0 "Q2" H 2300 2800 50  0000 L CNN
F 1 "2N3904" H 2341 2655 50  0001 L CNN
F 2 "Theremin:TO92-321" H 2350 2625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 2150 2700 50  0001 L CNN
	1    2150 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1550 2900 1550 2950
Wire Wire Line
	1550 2950 1800 2950
Wire Wire Line
	2050 2950 2050 2900
$Comp
L Device:R_Small R2
U 1 1 5CA85E3B
P 1800 3150
F 0 "R2" H 1859 3196 50  0000 L CNN
F 1 "2K2" H 1859 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 1800 3150 50  0001 C CNN
F 3 "~" H 1800 3150 50  0001 C CNN
	1    1800 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3050 1800 2950
Connection ~ 1800 2950
Wire Wire Line
	1800 2950 2050 2950
$Comp
L Device:R_Small R4
U 1 1 5CA86717
P 2400 2900
F 0 "R4" H 2459 2946 50  0000 L CNN
F 1 "1K" H 2459 2855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2400 2900 50  0001 C CNN
F 3 "~" H 2400 2900 50  0001 C CNN
	1    2400 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5CA87054
P 2400 2500
F 0 "R3" H 2459 2546 50  0000 L CNN
F 1 "47K" H 2459 2455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2400 2500 50  0001 C CNN
F 3 "~" H 2400 2500 50  0001 C CNN
	1    2400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2600 2400 2700
Wire Wire Line
	2350 2700 2400 2700
Connection ~ 2400 2700
Wire Wire Line
	2400 2700 2400 2800
$Comp
L power:GND #PWR0102
U 1 1 5CA886B8
P 2400 3300
F 0 "#PWR0102" H 2400 3050 50  0001 C CNN
F 1 "GND" H 2405 3127 50  0000 C CNN
F 2 "" H 2400 3300 50  0001 C CNN
F 3 "" H 2400 3300 50  0001 C CNN
	1    2400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3000 2400 3050
Wire Wire Line
	2650 2800 2650 3050
Wire Wire Line
	2650 3050 2400 3050
Connection ~ 2400 3050
Wire Wire Line
	2400 3050 2400 3300
Wire Wire Line
	2650 2600 2650 1500
Wire Wire Line
	2650 1500 2050 1500
Connection ~ 1550 1500
Wire Wire Line
	2050 2500 2050 1500
Connection ~ 2050 1500
Wire Wire Line
	2050 1500 1550 1500
Wire Wire Line
	1550 2300 1700 2300
Wire Wire Line
	2400 2300 2400 2400
$Comp
L power:+12V #PWR0103
U 1 1 5CA9448A
P 2050 1400
F 0 "#PWR0103" H 2050 1250 50  0001 C CNN
F 1 "+12V" H 2065 1573 50  0000 C CNN
F 2 "" H 2050 1400 50  0001 C CNN
F 3 "" H 2050 1400 50  0001 C CNN
	1    2050 1400
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR0104
U 1 1 5CA95545
P 1800 3450
F 0 "#PWR0104" H 1800 3550 50  0001 C CNN
F 1 "-12V" H 1815 3623 50  0000 C CNN
F 2 "" H 1800 3450 50  0001 C CNN
F 3 "" H 1800 3450 50  0001 C CNN
	1    1800 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	1800 3250 1800 3450
Wire Wire Line
	2050 1400 2050 1500
$Comp
L Device:C_Small C5
U 1 1 5CAA031E
P 3000 1900
F 0 "C5" H 3092 1946 50  0000 L CNN
F 1 "3n3" H 3092 1855 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3000 1900 50  0001 C CNN
F 3 "~" H 3000 1900 50  0001 C CNN
	1    3000 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R5
U 1 1 5CAA0328
P 3300 2900
F 0 "R5" H 3359 2946 50  0000 L CNN
F 1 "1K" H 3359 2855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 3300 2900 50  0001 C CNN
F 3 "~" H 3300 2900 50  0001 C CNN
	1    3300 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:LTRIM L6
U 1 1 5CAA0332
P 3650 1700
F 0 "L6" H 3753 1746 50  0000 L CNN
F 1 "47uH" H 3753 1655 50  0000 L CNN
F 2 "Theremin:Toko-1" H 3650 1700 50  0001 C CNN
F 3 "~" H 3650 1700 50  0001 C CNN
	1    3650 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C8
U 1 1 5CAA033C
P 4750 2700
F 0 "C8" H 4838 2746 50  0000 L CNN
F 1 "1uF" H 4838 2655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 4750 2700 50  0001 C CNN
F 3 "~" H 4750 2700 50  0001 C CNN
	1    4750 2700
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N3904 Q3
U 1 1 5CAA0346
P 3550 2700
F 0 "Q3" H 3700 2800 50  0000 L CNN
F 1 "2N3904" H 3750 2650 50  0000 L CNN
F 2 "Theremin:TO92-321" H 3750 2625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 3550 2700 50  0001 L CNN
	1    3550 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:L L13
U 1 1 5CAA0350
P 3650 2100
F 0 "L13" H 3700 2250 50  0000 L CNN
F 1 "47uH" H 3700 2150 50  0000 L CNN
F 2 "Inductor_THT:L_Axial_L9.5mm_D4.0mm_P15.24mm_Horizontal_Fastron_SMCC" H 3650 2100 50  0001 C CNN
F 3 "~" H 3650 2100 50  0001 C CNN
	1    3650 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5CAA035A
P 3000 2850
F 0 "C6" H 3050 2950 50  0000 L CNN
F 1 "15pF" H 3050 2750 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3000 2850 50  0001 C CNN
F 3 "~" H 3000 2850 50  0001 C CNN
	1    3000 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2250 3650 2300
Wire Wire Line
	3650 2300 3000 2300
Connection ~ 3650 2300
Wire Wire Line
	3650 2300 3650 2500
Wire Wire Line
	3000 2000 3000 2300
Connection ~ 3000 2300
Wire Wire Line
	3650 1850 3650 1950
Wire Wire Line
	3000 1800 3000 1500
Wire Wire Line
	3000 1500 3650 1500
Wire Wire Line
	3650 1500 3650 1550
Wire Wire Line
	3000 2750 3000 2300
Wire Wire Line
	3350 2700 3300 2700
Wire Wire Line
	3300 2700 3300 2800
$Comp
L power:GND #PWR0105
U 1 1 5CAA0374
P 3300 3300
F 0 "#PWR0105" H 3300 3050 50  0001 C CNN
F 1 "GND" H 3305 3127 50  0000 C CNN
F 2 "" H 3300 3300 50  0001 C CNN
F 3 "" H 3300 3300 50  0001 C CNN
	1    3300 3300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N3904 Q4
U 1 1 5CAA037F
P 4250 2700
F 0 "Q4" H 4400 2800 50  0000 L CNN
F 1 "2N3904" H 4441 2655 50  0001 L CNN
F 2 "Theremin:TO92-321" H 4450 2625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 4250 2700 50  0001 L CNN
	1    4250 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3650 2900 3650 2950
Wire Wire Line
	3650 2950 3900 2950
Wire Wire Line
	4150 2950 4150 2900
$Comp
L Device:R_Small R6
U 1 1 5CAA038C
P 3900 3150
F 0 "R6" H 3959 3196 50  0000 L CNN
F 1 "2K2" H 3959 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 3900 3150 50  0001 C CNN
F 3 "~" H 3900 3150 50  0001 C CNN
	1    3900 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3050 3900 2950
Connection ~ 3900 2950
Wire Wire Line
	3900 2950 4150 2950
$Comp
L Device:R_Small R8
U 1 1 5CAA0399
P 4500 2900
F 0 "R8" H 4559 2946 50  0000 L CNN
F 1 "1K" H 4559 2855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4500 2900 50  0001 C CNN
F 3 "~" H 4500 2900 50  0001 C CNN
	1    4500 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5CAA03A3
P 4500 2500
F 0 "R7" H 4559 2546 50  0000 L CNN
F 1 "47K" H 4559 2455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4500 2500 50  0001 C CNN
F 3 "~" H 4500 2500 50  0001 C CNN
	1    4500 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2600 4500 2700
Wire Wire Line
	4450 2700 4500 2700
Connection ~ 4500 2700
Wire Wire Line
	4500 2700 4500 2800
$Comp
L power:GND #PWR0106
U 1 1 5CAA03B1
P 4500 3300
F 0 "#PWR0106" H 4500 3050 50  0001 C CNN
F 1 "GND" H 4505 3127 50  0000 C CNN
F 2 "" H 4500 3300 50  0001 C CNN
F 3 "" H 4500 3300 50  0001 C CNN
	1    4500 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3000 4500 3050
Wire Wire Line
	4750 2800 4750 3050
Wire Wire Line
	4750 3050 4500 3050
Connection ~ 4500 3050
Wire Wire Line
	4750 2600 4750 1500
Wire Wire Line
	4750 1500 4150 1500
Connection ~ 3650 1500
Wire Wire Line
	4150 2500 4150 1500
Connection ~ 4150 1500
Wire Wire Line
	4150 1500 3650 1500
Wire Wire Line
	3650 2300 3800 2300
Wire Wire Line
	4500 2300 4500 2400
$Comp
L power:+12V #PWR0107
U 1 1 5CAA03C8
P 4150 1400
F 0 "#PWR0107" H 4150 1250 50  0001 C CNN
F 1 "+12V" H 4165 1573 50  0000 C CNN
F 2 "" H 4150 1400 50  0001 C CNN
F 3 "" H 4150 1400 50  0001 C CNN
	1    4150 1400
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR0108
U 1 1 5CAA03D2
P 3900 3450
F 0 "#PWR0108" H 3900 3550 50  0001 C CNN
F 1 "-12V" H 3915 3623 50  0000 C CNN
F 2 "" H 3900 3450 50  0001 C CNN
F 3 "" H 3900 3450 50  0001 C CNN
	1    3900 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	3900 3250 3900 3450
Wire Wire Line
	4150 1400 4150 1500
$Comp
L Device:CP_Small C19
U 1 1 5CAE748A
P 2350 6750
F 0 "C19" H 2438 6796 50  0000 L CNN
F 1 "2200uF" H 2400 6650 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D16.0mm_P7.50mm" H 2350 6750 50  0001 C CNN
F 3 "~" H 2350 6750 50  0001 C CNN
	1    2350 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C30
U 1 1 5CAE796B
P 2750 6750
F 0 "C30" H 2842 6796 50  0000 L CNN
F 1 "470nF" H 2842 6705 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 2750 6750 50  0001 C CNN
F 3 "~" H 2750 6750 50  0001 C CNN
	1    2750 6750
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L78L12_TO92 U1
U 1 1 5CAE7EE7
P 3350 6550
F 0 "U1" H 3350 6792 50  0000 C CNN
F 1 "L78L12_TO92" H 3350 6701 50  0000 C CNN
F 2 "Theremin:TO92-123" H 3350 6775 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/15/55/e5/aa/23/5b/43/fd/CD00000446.pdf/files/CD00000446.pdf/jcr:content/translations/en.CD00000446.pdf" H 3350 6500 50  0001 C CNN
	1    3350 6550
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L79L12_TO92 U2
U 1 1 5CAEB1AC
P 3350 7350
F 0 "U2" H 3350 7201 50  0000 C CNN
F 1 "L79L12_TO92" H 3350 7110 50  0000 C CNN
F 2 "Theremin:TO92-123" H 3350 7150 50  0001 C CIN
F 3 "http://www.farnell.com/datasheets/1827870.pdf" H 3350 7350 50  0001 C CNN
	1    3350 7350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C20
U 1 1 5CAF035F
P 2350 7150
F 0 "C20" H 2438 7196 50  0000 L CNN
F 1 "2200uF" H 2400 7050 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D16.0mm_P7.50mm" H 2350 7150 50  0001 C CNN
F 3 "~" H 2350 7150 50  0001 C CNN
	1    2350 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C29
U 1 1 5CAF2A9E
P 2750 7150
F 0 "C29" H 2842 7196 50  0000 L CNN
F 1 "470nF" H 2842 7105 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 2750 7150 50  0001 C CNN
F 3 "~" H 2750 7150 50  0001 C CNN
	1    2750 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C21
U 1 1 5CAF3B1F
P 3750 6750
F 0 "C21" H 3842 6796 50  0000 L CNN
F 1 "100nF" H 3842 6705 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3750 6750 50  0001 C CNN
F 3 "~" H 3750 6750 50  0001 C CNN
	1    3750 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C22
U 1 1 5CAF4190
P 3750 7150
F 0 "C22" H 3842 7196 50  0000 L CNN
F 1 "100nF" H 3842 7105 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3750 7150 50  0001 C CNN
F 3 "~" H 3750 7150 50  0001 C CNN
	1    3750 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6550 2350 6550
Wire Wire Line
	2250 7350 2350 7350
Wire Wire Line
	3350 6850 3350 6950
Wire Wire Line
	3750 6850 3750 6950
Wire Wire Line
	3750 7250 3750 7350
Wire Wire Line
	3750 7350 3650 7350
Wire Wire Line
	3750 6650 3750 6550
Wire Wire Line
	3750 6550 3650 6550
Wire Wire Line
	2750 6550 2750 6650
Connection ~ 2750 6550
Wire Wire Line
	2750 6550 3050 6550
Wire Wire Line
	2750 6850 2750 6950
Wire Wire Line
	2750 7250 2750 7350
Connection ~ 2750 7350
Wire Wire Line
	2750 7350 3050 7350
Wire Wire Line
	2350 6850 2350 6950
Wire Wire Line
	2350 7250 2350 7350
Connection ~ 2350 7350
Wire Wire Line
	2350 7350 2750 7350
Wire Wire Line
	2350 6650 2350 6550
Connection ~ 2350 6550
Wire Wire Line
	2350 6550 2750 6550
Wire Wire Line
	2350 6950 2750 6950
Connection ~ 2350 6950
Wire Wire Line
	2350 6950 2350 7050
Connection ~ 2750 6950
Wire Wire Line
	2750 6950 2750 7050
Wire Wire Line
	2750 6950 3350 6950
Connection ~ 3350 6950
Wire Wire Line
	3350 6950 3350 7050
Wire Wire Line
	3350 6950 3750 6950
Connection ~ 3750 6950
Wire Wire Line
	3750 6950 3750 7050
Wire Wire Line
	1950 7350 1850 7350
Wire Wire Line
	1850 7350 1850 6550
Wire Wire Line
	1850 6550 1950 6550
$Comp
L Connector:Barrel_Jack J1
U 1 1 5CB16E4A
P 800 6650
F 0 "J1" H 857 6975 50  0000 C CNN
F 1 "14V_200mA" H 857 6884 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 850 6610 50  0001 C CNN
F 3 "~" H 850 6610 50  0001 C CNN
	1    800  6650
	1    0    0    -1  
$EndComp
Connection ~ 1850 6550
Wire Wire Line
	1500 6950 2350 6950
$Comp
L power:-12V #PWR0109
U 1 1 5CB288A3
P 3950 7450
F 0 "#PWR0109" H 3950 7550 50  0001 C CNN
F 1 "-12V" H 3965 7623 50  0000 C CNN
F 2 "" H 3950 7450 50  0001 C CNN
F 3 "" H 3950 7450 50  0001 C CNN
	1    3950 7450
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5CB28D99
P 1500 7050
F 0 "#PWR0110" H 1500 6800 50  0001 C CNN
F 1 "GND" H 1505 6877 50  0000 C CNN
F 2 "" H 1500 7050 50  0001 C CNN
F 3 "" H 1500 7050 50  0001 C CNN
	1    1500 7050
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0111
U 1 1 5CB2A41B
P 3950 6450
F 0 "#PWR0111" H 3950 6300 50  0001 C CNN
F 1 "+12V" H 3965 6623 50  0000 C CNN
F 2 "" H 3950 6450 50  0001 C CNN
F 3 "" H 3950 6450 50  0001 C CNN
	1    3950 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 6550 3950 6550
Wire Wire Line
	3950 6550 3950 6450
Connection ~ 3750 6550
Wire Wire Line
	3750 7350 3950 7350
Wire Wire Line
	3950 7350 3950 7450
Connection ~ 3750 7350
Wire Wire Line
	1500 7050 1500 6950
Connection ~ 1500 6950
$Comp
L Amplifier_Operational:LM13600 U3
U 1 1 5CB41DCC
P 9300 5300
F 0 "U3" H 9400 5450 50  0000 C CNN
F 1 "LM13600" H 9450 5100 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 9000 5325 50  0001 C CNN
F 3 "http://pdf.datasheetcatalog.com/datasheet/nationalsemiconductor/DS007980.PDF" H 9000 5325 50  0001 C CNN
	1    9300 5300
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM13600 U3
U 2 1 5CB44EEE
P 10200 5300
F 0 "U3" H 10100 5648 50  0001 C CNN
F 1 "LM13600" H 10100 5557 50  0001 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 9900 5325 50  0001 C CNN
F 3 "http://pdf.datasheetcatalog.com/datasheet/nationalsemiconductor/DS007980.PDF" H 9900 5325 50  0001 C CNN
	2    10200 5300
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM13600 U3
U 3 1 5CB46ECE
P 5350 5100
F 0 "U3" H 5400 5250 50  0000 C CNN
F 1 "LM13600" H 5400 4900 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5050 5125 50  0001 C CNN
F 3 "http://pdf.datasheetcatalog.com/datasheet/nationalsemiconductor/DS007980.PDF" H 5050 5125 50  0001 C CNN
	3    5350 5100
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM13600 U3
U 4 1 5CB47EAA
P 6300 5100
F 0 "U3" H 6200 5357 50  0001 C CNN
F 1 "LM13600" H 6200 5357 50  0001 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 6000 5125 50  0001 C CNN
F 3 "http://pdf.datasheetcatalog.com/datasheet/nationalsemiconductor/DS007980.PDF" H 6000 5125 50  0001 C CNN
	4    6300 5100
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM13600 U3
U 5 1 5CB49634
P 9300 5300
F 0 "U3" H 9258 5346 50  0001 L CNN
F 1 "LM13600" H 9258 5255 50  0001 L CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 9000 5325 50  0001 C CNN
F 3 "http://pdf.datasheetcatalog.com/datasheet/nationalsemiconductor/DS007980.PDF" H 9000 5325 50  0001 C CNN
	5    9300 5300
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4001 D3
U 1 1 5CAE01A8
P 2100 7350
F 0 "D3" H 2100 7566 50  0000 C CNN
F 1 "1N4001" H 2100 7475 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2100 7175 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 2100 7350 50  0001 C CNN
	1    2100 7350
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4001 D2
U 1 1 5CAE0FDB
P 2100 6550
F 0 "D2" H 2100 6766 50  0000 C CNN
F 1 "1N4001" H 2100 6675 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2100 6375 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 2100 6550 50  0001 C CNN
	1    2100 6550
	-1   0    0    -1  
$EndComp
$Comp
L Diode:1N4148 D4
U 1 1 5CB5256A
P 3900 5200
F 0 "D4" V 3946 5121 50  0000 R CNN
F 1 "1N4148" V 3800 5200 50  0000 R CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3900 5025 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3900 5200 50  0001 C CNN
	1    3900 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R27
U 1 1 5CB54211
P 5400 4650
F 0 "R27" V 5300 4650 50  0000 C CNN
F 1 "150K" V 5500 4650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5400 4650 50  0001 C CNN
F 3 "~" H 5400 4650 50  0001 C CNN
	1    5400 4650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R25
U 1 1 5CB55956
P 4700 4450
F 0 "R25" V 4504 4450 50  0000 C CNN
F 1 "330K" V 4595 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4700 4450 50  0001 C CNN
F 3 "~" H 4700 4450 50  0001 C CNN
	1    4700 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R26
U 1 1 5CB55BB7
P 4850 5450
F 0 "R26" H 4900 5500 50  0000 L CNN
F 1 "4K7" H 4900 5400 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4850 5450 50  0001 C CNN
F 3 "~" H 4850 5450 50  0001 C CNN
	1    4850 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R30
U 1 1 5CB56882
P 5650 5600
F 0 "R30" H 5709 5646 50  0000 L CNN
F 1 "47K" H 5709 5555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5650 5600 50  0001 C CNN
F 3 "~" H 5650 5600 50  0001 C CNN
	1    5650 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C24
U 1 1 5CB56F63
P 6200 5500
F 0 "C24" H 6200 5600 50  0000 L CNN
F 1 "3n3" H 6200 5400 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 6200 5500 50  0001 C CNN
F 3 "~" H 6200 5500 50  0001 C CNN
	1    6200 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R31
U 1 1 5CB5745E
P 5900 5500
F 0 "R31" H 5959 5546 50  0000 L CNN
F 1 "4K7" H 5959 5455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5900 5500 50  0001 C CNN
F 3 "~" H 5900 5500 50  0001 C CNN
	1    5900 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C25
U 1 1 5CB577AE
P 6700 5200
F 0 "C25" V 6800 5150 50  0000 L CNN
F 1 "1uF" V 6600 5150 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 6700 5200 50  0001 C CNN
F 3 "~" H 6700 5200 50  0001 C CNN
	1    6700 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R33
U 1 1 5CB5825C
P 7000 5200
F 0 "R33" V 6900 5200 50  0000 C CNN
F 1 "4K7" V 7100 5200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 7000 5200 50  0001 C CNN
F 3 "~" H 7000 5200 50  0001 C CNN
	1    7000 5200
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C26
U 1 1 5CB58938
P 7200 5400
F 0 "C26" H 7200 5500 50  0000 L CNN
F 1 "10nF" H 7200 5300 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 7200 5400 50  0001 C CNN
F 3 "~" H 7200 5400 50  0001 C CNN
	1    7200 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R34
U 1 1 5CB58F6C
P 7500 5400
F 0 "R34" H 7559 5446 50  0000 L CNN
F 1 "4K7" H 7559 5355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 7500 5400 50  0001 C CNN
F 3 "~" H 7500 5400 50  0001 C CNN
	1    7500 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 5200 6500 5200
Wire Wire Line
	6800 5200 6900 5200
$Comp
L Device:R_Small R32
U 1 1 5CB747E9
P 6500 5500
F 0 "R32" H 6559 5546 50  0000 L CNN
F 1 "4K7" H 6559 5455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6500 5500 50  0001 C CNN
F 3 "~" H 6500 5500 50  0001 C CNN
	1    6500 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5400 6500 5200
Connection ~ 6500 5200
Wire Wire Line
	6500 5200 6600 5200
Wire Wire Line
	4950 5100 4950 4650
Wire Wire Line
	4950 4650 5300 4650
Wire Wire Line
	4950 5100 5050 5100
Wire Wire Line
	5050 5000 4850 5000
Wire Wire Line
	4850 5000 4850 4450
Wire Wire Line
	4850 4450 5000 4450
$Comp
L Device:R_Small R28
U 1 1 5CB551B2
P 5100 4450
F 0 "R28" V 4900 4450 50  0000 C CNN
F 1 "150K" V 5000 4350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5100 4450 50  0001 C CNN
F 3 "~" H 5100 4450 50  0001 C CNN
	1    5100 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 4450 4850 4450
Connection ~ 4850 4450
Wire Wire Line
	4850 5350 4850 5000
Connection ~ 4850 5000
$Comp
L Device:R_Small R23
U 1 1 5CB90289
P 4250 5000
F 0 "R23" H 4309 5046 50  0000 L CNN
F 1 "10K" H 4309 4955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4250 5000 50  0001 C CNN
F 3 "~" H 4250 5000 50  0001 C CNN
	1    4250 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R24
U 1 1 5CB9057B
P 4250 5400
F 0 "R24" H 4300 5450 50  0000 L CNN
F 1 "4K7" H 4250 5250 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4250 5400 50  0001 C CNN
F 3 "~" H 4250 5400 50  0001 C CNN
	1    4250 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C23
U 1 1 5CB90A15
P 4500 5450
F 0 "C23" H 4592 5496 50  0000 L CNN
F 1 "4n7" H 4592 5405 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 4500 5450 50  0001 C CNN
F 3 "~" H 4500 5450 50  0001 C CNN
	1    4500 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 5200 4500 5200
Wire Wire Line
	4250 5200 4250 5300
Wire Wire Line
	4250 5200 4250 5100
Connection ~ 4250 5200
Wire Wire Line
	4250 5500 4250 5650
Wire Wire Line
	4250 5650 3900 5650
Wire Wire Line
	3900 5650 3900 5350
Wire Wire Line
	4250 5650 4500 5650
Wire Wire Line
	4500 5650 4500 5550
Connection ~ 4250 5650
Wire Wire Line
	4500 5350 4500 5200
Connection ~ 4500 5200
Wire Wire Line
	4500 5200 4250 5200
Wire Wire Line
	3900 5050 3900 4700
Wire Wire Line
	3900 4700 4250 4700
Wire Wire Line
	4250 4700 4250 4900
$Comp
L power:GND #PWR0112
U 1 1 5CBCD2BD
P 4250 5750
F 0 "#PWR0112" H 4250 5500 50  0001 C CNN
F 1 "GND" H 4255 5577 50  0000 C CNN
F 2 "" H 4250 5750 50  0001 C CNN
F 3 "" H 4250 5750 50  0001 C CNN
	1    4250 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5CBCD8F8
P 4850 5750
F 0 "#PWR0113" H 4850 5500 50  0001 C CNN
F 1 "GND" H 4855 5577 50  0000 C CNN
F 2 "" H 4850 5750 50  0001 C CNN
F 3 "" H 4850 5750 50  0001 C CNN
	1    4850 5750
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR0114
U 1 1 5CC17ACF
P 4400 4450
F 0 "#PWR0114" H 4400 4550 50  0001 C CNN
F 1 "-12V" V 4415 4578 50  0000 L CNN
F 2 "" H 4400 4450 50  0001 C CNN
F 3 "" H 4400 4450 50  0001 C CNN
	1    4400 4450
	0    -1   1    0   
$EndComp
Wire Wire Line
	4400 4450 4600 4450
Wire Wire Line
	4250 5750 4250 5650
Wire Wire Line
	4850 5750 4850 5550
$Comp
L power:-12V #PWR0115
U 1 1 5CC3BAD0
P 5350 6100
F 0 "#PWR0115" H 5350 6200 50  0001 C CNN
F 1 "-12V" H 5450 6250 50  0000 C CNN
F 2 "" H 5350 6100 50  0001 C CNN
F 3 "" H 5350 6100 50  0001 C CNN
	1    5350 6100
	1    0    0    1   
$EndComp
Wire Wire Line
	5650 5100 5900 5100
Wire Wire Line
	5900 5600 5900 5700
Wire Wire Line
	5900 5700 6050 5700
Wire Wire Line
	6200 5700 6200 5600
$Comp
L power:GND #PWR0116
U 1 1 5CC48ED3
P 6050 5800
F 0 "#PWR0116" H 6050 5550 50  0001 C CNN
F 1 "GND" H 6055 5627 50  0001 C CNN
F 2 "" H 6050 5800 50  0001 C CNN
F 3 "" H 6050 5800 50  0001 C CNN
	1    6050 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 5800 6050 5700
Connection ~ 6050 5700
Wire Wire Line
	6050 5700 6200 5700
Wire Wire Line
	6200 5400 6200 5300
Wire Wire Line
	6200 5300 5900 5300
Wire Wire Line
	5900 5300 5900 5400
Wire Wire Line
	5900 5300 5900 5100
Connection ~ 5900 5300
Connection ~ 5900 5100
Wire Wire Line
	5900 5100 6000 5100
Wire Wire Line
	7100 5200 7200 5200
Wire Wire Line
	7200 5200 7200 5300
Wire Wire Line
	7500 5300 7500 5200
Wire Wire Line
	7500 5200 7200 5200
Connection ~ 7200 5200
Wire Wire Line
	7200 5500 7200 5600
Wire Wire Line
	7500 5600 7500 5500
$Comp
L power:GND #PWR0117
U 1 1 5CCBF5F6
P 7500 5700
F 0 "#PWR0117" H 7500 5450 50  0001 C CNN
F 1 "GND" H 7505 5527 50  0001 C CNN
F 2 "" H 7500 5700 50  0001 C CNN
F 3 "" H 7500 5700 50  0001 C CNN
	1    7500 5700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Male J3
U 1 1 5CCD9709
P 11050 5800
F 0 "J3" H 11022 5682 50  0000 R CNN
F 1 "Conn_01x08_Male" H 11022 5773 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 11050 5800 50  0001 C CNN
F 3 "~" H 11050 5800 50  0001 C CNN
	1    11050 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 5700 5650 6000
$Comp
L Device:C_Small C28
U 1 1 5CCEB01E
P 9800 6150
F 0 "C28" H 9850 6200 50  0000 L CNN
F 1 "100nF" H 9850 6100 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 9800 6150 50  0001 C CNN
F 3 "~" H 9800 6150 50  0001 C CNN
	1    9800 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5CCF7653
P 9800 6350
F 0 "#PWR0118" H 9800 6100 50  0001 C CNN
F 1 "GND" H 9805 6177 50  0001 C CNN
F 2 "" H 9800 6350 50  0001 C CNN
F 3 "" H 9800 6350 50  0001 C CNN
	1    9800 6350
	1    0    0    -1  
$EndComp
Connection ~ 3900 4700
Wire Wire Line
	5650 6000 9800 6000
Wire Wire Line
	5650 5200 5650 5400
$Comp
L Device:R_Small R29
U 1 1 5CB56508
P 5350 5600
F 0 "R29" H 5500 5600 50  0000 C CNN
F 1 "100K" H 5500 5500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5350 5600 50  0001 C CNN
F 3 "~" H 5350 5600 50  0001 C CNN
	1    5350 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 5700 5350 6100
Wire Wire Line
	5350 5500 5350 5400
Wire Wire Line
	5350 5400 5650 5400
Connection ~ 5650 5400
Wire Wire Line
	5650 5400 5650 5500
$Comp
L Device:R_Small R35
U 1 1 5CD4B179
P 8550 4900
F 0 "R35" H 8650 4900 50  0000 C CNN
F 1 "4M7" H 8650 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 8550 4900 50  0001 C CNN
F 3 "~" H 8550 4900 50  0001 C CNN
	1    8550 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R36
U 1 1 5CD4B852
P 8900 4900
F 0 "R36" H 9000 4900 50  0000 C CNN
F 1 "1M" H 9000 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 8900 4900 50  0001 C CNN
F 3 "~" H 8900 4900 50  0001 C CNN
	1    8900 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C27
U 1 1 5CD4BC0A
P 8750 5400
F 0 "C27" V 8650 5400 50  0000 C CNN
F 1 "100nF" V 8850 5400 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 8750 5400 50  0001 C CNN
F 3 "~" H 8750 5400 50  0001 C CNN
	1    8750 5400
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R37
U 1 1 5CD4C978
P 8700 5900
F 0 "R37" V 8500 5900 50  0000 C CNN
F 1 "3M3" V 8600 5900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 8700 5900 50  0001 C CNN
F 3 "~" H 8700 5900 50  0001 C CNN
	1    8700 5900
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148 D5
U 1 1 5CD4CF48
P 9000 5900
F 0 "D5" H 9050 5700 50  0000 R CNN
F 1 "1N4148" H 9150 5800 50  0000 R CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9000 5725 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9000 5900 50  0001 C CNN
	1    9000 5900
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R38
U 1 1 5CD4EC4D
P 9700 5100
F 0 "R38" H 9800 5100 50  0000 C CNN
F 1 "47K" H 9800 5000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9700 5100 50  0001 C CNN
F 3 "~" H 9700 5100 50  0001 C CNN
	1    9700 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 6050 9800 6000
Connection ~ 9800 6000
Wire Wire Line
	9800 6250 9800 6300
Wire Wire Line
	10850 6100 10300 6100
Wire Wire Line
	10300 6100 10300 6300
Wire Wire Line
	10300 6300 9800 6300
Connection ~ 9800 6300
Wire Wire Line
	9800 6300 9800 6350
Text Label 10550 6000 0    50   ~ 0
VCA_OUT
Text Label 10550 6100 0    50   ~ 0
GND
Wire Wire Line
	9900 5900 9900 5300
Text Label 10550 5900 0    50   ~ 0
AMP_IN
Text Label 10550 5800 0    50   ~ 0
AMP_OUT
Wire Wire Line
	10850 5800 10300 5800
Wire Wire Line
	10300 5800 10300 5400
Text Label 10550 5700 0    50   ~ 0
-12V
Wire Wire Line
	9600 5300 9800 5300
Wire Wire Line
	9800 5300 9800 5900
Wire Wire Line
	10850 5700 9200 5700
Wire Wire Line
	9200 5700 9200 5600
Wire Wire Line
	9600 5400 9700 5400
Wire Wire Line
	9700 5400 9700 5200
$Comp
L power:GND #PWR0119
U 1 1 5CE1AD5E
P 8900 5500
F 0 "#PWR0119" H 8900 5250 50  0001 C CNN
F 1 "GND" H 8905 5327 50  0001 C CNN
F 2 "" H 8900 5500 50  0001 C CNN
F 3 "" H 8900 5500 50  0001 C CNN
	1    8900 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 5300 8900 5300
Wire Wire Line
	8900 5300 8900 5000
Wire Wire Line
	8850 5400 8900 5400
Wire Wire Line
	8900 5500 8900 5400
Connection ~ 8900 5400
Wire Wire Line
	8900 5400 9000 5400
Wire Wire Line
	8800 5900 8850 5900
Wire Wire Line
	9150 5900 9800 5900
Connection ~ 9800 5900
Wire Wire Line
	9800 5900 9800 6000
Wire Wire Line
	8600 5900 8550 5900
Wire Wire Line
	8550 5900 8550 5400
Wire Wire Line
	8550 5400 8650 5400
Wire Wire Line
	8550 5400 8550 5200
Connection ~ 8550 5400
Wire Wire Line
	8550 4800 8550 4700
Wire Wire Line
	8550 4700 8900 4700
Wire Wire Line
	8900 4700 8900 4800
Wire Wire Line
	8900 4700 9200 4700
Wire Wire Line
	9700 4700 9700 5000
Connection ~ 8900 4700
Wire Wire Line
	10400 4700 9700 4700
Connection ~ 9700 4700
Wire Wire Line
	9200 5000 9200 4700
Connection ~ 9200 4700
Wire Wire Line
	9200 4700 9700 4700
Wire Wire Line
	9000 5200 8550 5200
Connection ~ 8550 5200
Wire Wire Line
	8550 5200 8550 5000
Wire Wire Line
	10850 5600 10400 5600
Wire Wire Line
	10400 4700 10400 5600
Text Label 10550 5600 0    50   ~ 0
+12V
Wire Wire Line
	4500 5200 4500 4050
Wire Wire Line
	10500 4050 10500 5500
Wire Wire Line
	10500 5500 10850 5500
Text Label 10550 5500 0    50   ~ 0
AUDIO
$Comp
L power:+12V #PWR0120
U 1 1 5CEA5CB5
P 9200 4600
F 0 "#PWR0120" H 9200 4450 50  0001 C CNN
F 1 "+12V" H 9215 4773 50  0000 C CNN
F 2 "" H 9200 4600 50  0001 C CNN
F 3 "" H 9200 4600 50  0001 C CNN
	1    9200 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 4600 9200 4700
$Comp
L power:-12V #PWR0121
U 1 1 5CEC6B6D
P 9200 6100
F 0 "#PWR0121" H 9200 6200 50  0001 C CNN
F 1 "-12V" H 9215 6273 50  0000 C CNN
F 2 "" H 9200 6100 50  0001 C CNN
F 3 "" H 9200 6100 50  0001 C CNN
	1    9200 6100
	1    0    0    1   
$EndComp
Wire Wire Line
	9200 6100 9200 5700
Connection ~ 9200 5700
$Comp
L Device:L L1
U 1 1 5CA73142
P 1950 1050
F 0 "L1" V 2140 1050 50  0000 C CNN
F 1 "10mH" V 2049 1050 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L30.0mm_D8.0mm_P35.56mm_Horizontal_Fastron_77A" H 1950 1050 50  0001 C CNN
F 3 "~" H 1950 1050 50  0001 C CNN
	1    1950 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	900  4700 3000 4700
Wire Wire Line
	3000 2950 3000 4700
Connection ~ 3000 4700
Wire Wire Line
	3000 4700 3900 4700
Wire Wire Line
	900  2950 900  4700
Wire Wire Line
	1000 1050 900  1050
Wire Wire Line
	900  1050 900  1150
Wire Wire Line
	2200 1050 2100 1050
Wire Wire Line
	2200 950  2200 1050
Wire Wire Line
	1800 1050 1700 1050
Wire Wire Line
	1400 1050 1300 1050
$Comp
L Transistor_BJT:2N3904 Q5
U 1 1 5CFC8515
P 5650 2550
F 0 "Q5" H 5800 2650 50  0000 L CNN
F 1 "2N3904" H 5300 2450 50  0000 L CNN
F 2 "Theremin:TO92-321" H 5850 2475 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 5650 2550 50  0001 L CNN
	1    5650 2550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5CFC8C8F
P 6200 2750
F 0 "R11" H 6259 2796 50  0000 L CNN
F 1 "33" H 6259 2705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6200 2750 50  0001 C CNN
F 3 "~" H 6200 2750 50  0001 C CNN
	1    6200 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R10
U 1 1 5CFC918C
P 6000 2550
F 0 "R10" V 5804 2550 50  0000 C CNN
F 1 "470" V 5895 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6000 2550 50  0001 C CNN
F 3 "~" H 6000 2550 50  0001 C CNN
	1    6000 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R12
U 1 1 5CFC9802
P 6200 3150
F 0 "R12" H 6259 3196 50  0000 L CNN
F 1 "10K" H 6259 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6200 3150 50  0001 C CNN
F 3 "~" H 6200 3150 50  0001 C CNN
	1    6200 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5CFC9BA9
P 5550 3150
F 0 "R9" H 5609 3196 50  0000 L CNN
F 1 "10K" H 5609 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5550 3150 50  0001 C CNN
F 3 "~" H 5550 3150 50  0001 C CNN
	1    5550 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R39
U 1 1 5CFC9DF6
P 5150 3150
F 0 "R39" H 5209 3196 50  0000 L CNN
F 1 "10K" H 5209 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5150 3150 50  0001 C CNN
F 3 "~" H 5150 3150 50  0001 C CNN
	1    5150 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5CFCA0C5
P 6200 2350
F 0 "C9" H 6292 2396 50  0000 L CNN
F 1 "33pF" H 6292 2305 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 6200 2350 50  0001 C CNN
F 3 "~" H 6200 2350 50  0001 C CNN
	1    6200 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 5CFCA880
P 5150 2700
F 0 "C11" H 5242 2746 50  0000 L CNN
F 1 "470nF" H 5242 2655 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 5150 2700 50  0001 C CNN
F 3 "~" H 5150 2700 50  0001 C CNN
	1    5150 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R13
U 1 1 5CFCB036
P 6500 3150
F 0 "R13" H 6350 3100 50  0000 C CNN
F 1 "2K2" H 6350 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6500 3150 50  0001 C CNN
F 3 "~" H 6500 3150 50  0001 C CNN
	1    6500 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 2750 5550 2950
Wire Wire Line
	5150 3050 5150 2950
Wire Wire Line
	5150 2950 5550 2950
Connection ~ 5550 2950
Wire Wire Line
	5550 2950 5550 3050
Wire Wire Line
	5850 2550 5900 2550
Wire Wire Line
	6100 2550 6200 2550
Wire Wire Line
	6200 2550 6200 2650
Wire Wire Line
	6200 2550 6200 2450
Connection ~ 6200 2550
Wire Wire Line
	6200 2250 6200 2100
Wire Wire Line
	6200 2100 5550 2100
Wire Wire Line
	5550 2100 5550 2350
Wire Wire Line
	1900 2300 2400 2300
$Comp
L Device:C_Small C7
U 1 1 5D014D75
P 3950 2300
F 0 "C7" V 3850 2300 50  0000 C CNN
F 1 "10nF" V 4050 2300 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3950 2300 50  0001 C CNN
F 3 "~" H 3950 2300 50  0001 C CNN
	1    3950 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 2300 4500 2300
$Comp
L Device:C_Small C3
U 1 1 5D0144B8
P 1800 2300
F 0 "C3" V 1750 2400 50  0000 C CNN
F 1 "10nF" V 1900 2300 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 1800 2300 50  0001 C CNN
F 3 "~" H 1800 2300 50  0001 C CNN
	1    1800 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 2100 3800 2100
Wire Wire Line
	3800 2100 3800 2300
Connection ~ 5550 2100
Connection ~ 3800 2300
Wire Wire Line
	3800 2300 3850 2300
$Comp
L power:+12V #PWR0122
U 1 1 5D07CF66
P 5150 2500
F 0 "#PWR0122" H 5150 2350 50  0001 C CNN
F 1 "+12V" H 5165 2673 50  0000 C CNN
F 2 "" H 5150 2500 50  0001 C CNN
F 3 "" H 5150 2500 50  0001 C CNN
	1    5150 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2500 5150 2600
Wire Wire Line
	5150 2950 5150 2800
Connection ~ 5150 2950
$Comp
L Device:C_Small C10
U 1 1 5D098656
P 5850 2950
F 0 "C10" V 5750 2950 50  0000 C CNN
F 1 "470nF" V 5950 2950 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 5850 2950 50  0001 C CNN
F 3 "~" H 5850 2950 50  0001 C CNN
	1    5850 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 2950 5750 2950
Wire Wire Line
	6200 2850 6200 2950
Wire Wire Line
	5950 2950 6200 2950
Connection ~ 6200 2950
Wire Wire Line
	6200 2950 6200 3050
Wire Wire Line
	5150 3250 5150 3350
Wire Wire Line
	5150 3350 6200 3350
Wire Wire Line
	6200 3350 6200 3250
$Comp
L power:GND #PWR0123
U 1 1 5D0EE288
P 6500 3300
F 0 "#PWR0123" H 6500 3050 50  0001 C CNN
F 1 "GND" H 6600 3150 50  0000 R CNN
F 2 "" H 6500 3300 50  0001 C CNN
F 3 "" H 6500 3300 50  0001 C CNN
	1    6500 3300
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR0124
U 1 1 5D0FD24F
P 5150 3450
F 0 "#PWR0124" H 5150 3550 50  0001 C CNN
F 1 "-12V" H 5165 3623 50  0000 C CNN
F 2 "" H 5150 3450 50  0001 C CNN
F 3 "" H 5150 3450 50  0001 C CNN
	1    5150 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	5150 3450 5150 3350
Connection ~ 5150 3350
$Comp
L Device:C_Small C14
U 1 1 5D2465CF
P 8650 2500
F 0 "C14" H 8650 2600 50  0000 L CNN
F 1 "1n8" H 8650 2400 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 8650 2500 50  0001 C CNN
F 3 "~" H 8650 2500 50  0001 C CNN
	1    8650 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R15
U 1 1 5D2465D5
P 7450 2950
F 0 "R15" H 7509 2996 50  0000 L CNN
F 1 "470" H 7509 2905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 7450 2950 50  0001 C CNN
F 3 "~" H 7450 2950 50  0001 C CNN
	1    7450 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:LTRIM L11
U 1 1 5D2465DB
P 7800 1700
F 0 "L11" H 7903 1746 50  0000 L CNN
F 1 "47uH" H 7903 1655 50  0000 L CNN
F 2 "Theremin:Toko-1" H 7800 1700 50  0001 C CNN
F 3 "~" H 7800 1700 50  0001 C CNN
	1    7800 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C13
U 1 1 5D2465E1
P 7250 2550
F 0 "C13" H 7338 2596 50  0000 L CNN
F 1 "22uF" H 7338 2505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P5.00mm" H 7250 2550 50  0001 C CNN
F 3 "~" H 7250 2550 50  0001 C CNN
	1    7250 2550
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N3904 Q6
U 1 1 5D2465E7
P 7700 2700
F 0 "Q6" H 7850 2800 50  0000 L CNN
F 1 "2N3904" H 7900 2600 50  0000 L CNN
F 2 "Theremin:TO92-321" H 7900 2625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 7700 2700 50  0001 L CNN
	1    7700 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:L L14
U 1 1 5D2465ED
P 7800 2100
F 0 "L14" H 7850 2250 50  0000 L CNN
F 1 "22uH" H 7850 2150 50  0000 L CNN
F 2 "Inductor_THT:L_Axial_L9.5mm_D4.0mm_P15.24mm_Horizontal_Fastron_SMCC" H 7800 2100 50  0001 C CNN
F 3 "~" H 7800 2100 50  0001 C CNN
	1    7800 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C15
U 1 1 5D2465F3
P 8650 2900
F 0 "C15" H 8650 3000 50  0000 L CNN
F 1 "6n8" H 8650 2800 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 8650 2900 50  0001 C CNN
F 3 "~" H 8650 2900 50  0001 C CNN
	1    8650 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 2250 7800 2400
Connection ~ 7800 2400
Wire Wire Line
	7800 2400 7800 2500
Wire Wire Line
	7800 1850 7800 1950
Wire Wire Line
	7800 1500 7800 1550
Wire Wire Line
	7500 2700 7450 2700
Wire Wire Line
	7450 2700 7450 2850
$Comp
L power:GND #PWR0125
U 1 1 5D246607
P 7450 3300
F 0 "#PWR0125" H 7450 3050 50  0001 C CNN
F 1 "GND" H 7455 3127 50  0000 C CNN
F 2 "" H 7450 3300 50  0001 C CNN
F 3 "" H 7450 3300 50  0001 C CNN
	1    7450 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3050 7450 3150
$Comp
L Transistor_BJT:2N3904 Q7
U 1 1 5D24660E
P 8400 2700
F 0 "Q7" H 8550 2800 50  0000 L CNN
F 1 "2N3904" H 8591 2655 50  0001 L CNN
F 2 "Theremin:TO92-321" H 8600 2625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 8400 2700 50  0001 L CNN
	1    8400 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7800 2900 7800 2950
Wire Wire Line
	7800 2950 8050 2950
Wire Wire Line
	8300 2950 8300 2900
$Comp
L Device:R_Small R16
U 1 1 5D246617
P 8050 3150
F 0 "R16" H 8109 3196 50  0000 L CNN
F 1 "2K2" H 8109 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 8050 3150 50  0001 C CNN
F 3 "~" H 8050 3150 50  0001 C CNN
	1    8050 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 3050 8050 2950
Connection ~ 8050 2950
Wire Wire Line
	8050 2950 8300 2950
$Comp
L Device:R_Small R17
U 1 1 5D246626
P 8900 2900
F 0 "R17" H 8959 2946 50  0000 L CNN
F 1 "470" H 8959 2855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 8900 2900 50  0001 C CNN
F 3 "~" H 8900 2900 50  0001 C CNN
	1    8900 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 2600 8650 2700
Wire Wire Line
	8600 2700 8650 2700
Connection ~ 8650 2700
Wire Wire Line
	8650 2700 8650 2800
$Comp
L power:GND #PWR0126
U 1 1 5D246630
P 8650 3300
F 0 "#PWR0126" H 8650 3050 50  0001 C CNN
F 1 "GND" H 8655 3127 50  0000 C CNN
F 2 "" H 8650 3300 50  0001 C CNN
F 3 "" H 8650 3300 50  0001 C CNN
	1    8650 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3000 8650 3100
Wire Wire Line
	8900 3100 8650 3100
Connection ~ 8650 3100
Wire Wire Line
	8650 3100 8650 3300
Wire Wire Line
	8300 2500 8300 1500
Connection ~ 8300 1500
Wire Wire Line
	8300 1500 7800 1500
Wire Wire Line
	7800 2400 8050 2400
$Comp
L power:+12V #PWR0127
U 1 1 5D246643
P 8300 1400
F 0 "#PWR0127" H 8300 1250 50  0001 C CNN
F 1 "+12V" H 8315 1573 50  0000 C CNN
F 2 "" H 8300 1400 50  0001 C CNN
F 3 "" H 8300 1400 50  0001 C CNN
	1    8300 1400
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR0128
U 1 1 5D246649
P 8050 3450
F 0 "#PWR0128" H 8050 3550 50  0001 C CNN
F 1 "-12V" H 8065 3623 50  0000 C CNN
F 2 "" H 8050 3450 50  0001 C CNN
F 3 "" H 8050 3450 50  0001 C CNN
	1    8050 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	8050 3250 8050 3450
Wire Wire Line
	8300 1400 8300 1500
$Comp
L Transistor_BJT:2N3904 Q8
U 1 1 5D246652
P 9800 2550
F 0 "Q8" H 9950 2650 50  0000 L CNN
F 1 "2N3904" H 9450 2450 50  0000 L CNN
F 2 "Theremin:TO92-321" H 10000 2475 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 9800 2550 50  0001 L CNN
	1    9800 2550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R20
U 1 1 5D246658
P 10350 2750
F 0 "R20" H 10409 2796 50  0000 L CNN
F 1 "33" H 10409 2705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 10350 2750 50  0001 C CNN
F 3 "~" H 10350 2750 50  0001 C CNN
	1    10350 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R19
U 1 1 5D24665E
P 10150 2550
F 0 "R19" V 9954 2550 50  0000 C CNN
F 1 "470" V 10045 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 10150 2550 50  0001 C CNN
F 3 "~" H 10150 2550 50  0001 C CNN
	1    10150 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R22
U 1 1 5D246664
P 10350 3150
F 0 "R22" H 10409 3196 50  0000 L CNN
F 1 "10K" H 10409 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 10350 3150 50  0001 C CNN
F 3 "~" H 10350 3150 50  0001 C CNN
	1    10350 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R18
U 1 1 5D24666A
P 9700 3150
F 0 "R18" H 9759 3196 50  0000 L CNN
F 1 "2K7" H 9759 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9700 3150 50  0001 C CNN
F 3 "~" H 9700 3150 50  0001 C CNN
	1    9700 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R40
U 1 1 5D246670
P 9450 3150
F 0 "R40" H 9509 3196 50  0000 L CNN
F 1 "10K" H 9509 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9450 3150 50  0001 C CNN
F 3 "~" H 9450 3150 50  0001 C CNN
	1    9450 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C16
U 1 1 5D246676
P 10350 2350
F 0 "C16" H 10442 2396 50  0000 L CNN
F 1 "22pF" H 10442 2305 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 10350 2350 50  0001 C CNN
F 3 "~" H 10350 2350 50  0001 C CNN
	1    10350 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R21
U 1 1 5D246682
P 10650 3150
F 0 "R21" H 10500 3100 50  0000 C CNN
F 1 "2K2" H 10500 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 10650 3150 50  0001 C CNN
F 3 "~" H 10650 3150 50  0001 C CNN
	1    10650 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	9700 2750 9700 2950
Wire Wire Line
	9450 3050 9450 2950
Wire Wire Line
	9450 2950 9700 2950
Connection ~ 9700 2950
Wire Wire Line
	9700 2950 9700 3050
Wire Wire Line
	10000 2550 10050 2550
Wire Wire Line
	10250 2550 10350 2550
Wire Wire Line
	10350 2550 10350 2650
Wire Wire Line
	10350 2550 10350 2450
Connection ~ 10350 2550
Wire Wire Line
	10350 2250 10350 2100
Wire Wire Line
	10350 2100 9700 2100
Wire Wire Line
	9700 2100 9700 2350
Wire Wire Line
	9700 2100 8650 2100
Wire Wire Line
	8050 2100 8050 2400
Connection ~ 9700 2100
Wire Wire Line
	9700 2950 10000 2950
Wire Wire Line
	10350 2850 10350 2950
Wire Wire Line
	10200 2950 10350 2950
Connection ~ 10350 2950
Wire Wire Line
	10350 2950 10350 3050
Wire Wire Line
	9450 3250 9450 3350
Wire Wire Line
	9450 3350 10350 3350
Wire Wire Line
	10350 3350 10350 3250
$Comp
L power:GND #PWR0129
U 1 1 5D2466B9
P 10650 3300
F 0 "#PWR0129" H 10650 3050 50  0001 C CNN
F 1 "GND" H 10750 3150 50  0000 R CNN
F 2 "" H 10650 3300 50  0001 C CNN
F 3 "" H 10650 3300 50  0001 C CNN
	1    10650 3300
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR0130
U 1 1 5D2466C0
P 9450 3450
F 0 "#PWR0130" H 9450 3550 50  0001 C CNN
F 1 "-12V" H 9465 3623 50  0000 C CNN
F 2 "" H 9450 3450 50  0001 C CNN
F 3 "" H 9450 3450 50  0001 C CNN
	1    9450 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	9450 3450 9450 3350
Connection ~ 9450 3350
$Comp
L Device:L L9
U 1 1 5D267C2C
P 9950 1050
F 0 "L9" V 10050 1050 50  0000 C CNN
F 1 "5mH" V 9850 1050 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L9.5mm_D4.0mm_P15.24mm_Horizontal_Fastron_SMCC" H 9950 1050 50  0001 C CNN
F 3 "~" H 9950 1050 50  0001 C CNN
	1    9950 1050
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L8
U 1 1 5D267C32
P 9550 1050
F 0 "L8" V 9650 1050 50  0000 C CNN
F 1 "2,5mH" V 9450 1050 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L9.5mm_D4.0mm_P15.24mm_Horizontal_Fastron_SMCC" H 9550 1050 50  0001 C CNN
F 3 "~" H 9550 1050 50  0001 C CNN
	1    9550 1050
	0    -1   -1   0   
$EndComp
$Comp
L Device:Antenna AE2
U 1 1 5D267C38
P 10600 750
F 0 "AE2" H 10680 739 50  0000 L CNN
F 1 "Volume antenna" H 10600 650 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D4.0mm_Drill2.0mm" H 10600 750 50  0001 C CNN
F 3 "~" H 10600 750 50  0001 C CNN
	1    10600 750 
	1    0    0    -1  
$EndComp
$Comp
L Device:L L10
U 1 1 5D267C3E
P 10350 1050
F 0 "L10" V 10450 1050 50  0000 C CNN
F 1 "5mH" V 10250 1050 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L9.5mm_D4.0mm_P15.24mm_Horizontal_Fastron_SMCC" H 10350 1050 50  0001 C CNN
F 3 "~" H 10350 1050 50  0001 C CNN
	1    10350 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10600 1050 10500 1050
Wire Wire Line
	10600 950  10600 1050
Wire Wire Line
	10200 1050 10100 1050
Wire Wire Line
	9800 1050 9700 1050
$Comp
L Device:L L7
U 1 1 5D27DDAD
P 9150 1050
F 0 "L7" V 9250 1050 50  0000 C CNN
F 1 "2,5mH" V 9050 1050 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L9.5mm_D4.0mm_P15.24mm_Horizontal_Fastron_SMCC" H 9150 1050 50  0001 C CNN
F 3 "~" H 9150 1050 50  0001 C CNN
	1    9150 1050
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D1
U 1 1 5D27E227
P 9150 750
F 0 "D1" H 9200 550 50  0000 R CNN
F 1 "1N4148" H 9300 650 50  0000 R CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9150 575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9150 750 50  0001 C CNN
	1    9150 750 
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C12
U 1 1 5D2813B1
P 8900 900
F 0 "C12" H 8650 900 50  0000 L CNN
F 1 "1nF" H 8650 800 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 8900 900 50  0001 C CNN
F 3 "~" H 8900 900 50  0001 C CNN
	1    8900 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R14
U 1 1 5D2818B5
P 8700 750
F 0 "R14" V 8504 750 50  0000 C CNN
F 1 "1M" V 8595 750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 8700 750 50  0001 C CNN
F 3 "~" H 8700 750 50  0001 C CNN
	1    8700 750 
	0    1    1    0   
$EndComp
Wire Wire Line
	8900 2700 8900 2800
Wire Wire Line
	8900 3000 8900 3100
Wire Wire Line
	7450 3150 7250 3150
Wire Wire Line
	7250 3150 7250 2650
Connection ~ 7450 3150
Wire Wire Line
	7450 3150 7450 3300
Wire Wire Line
	7800 1500 7250 1500
Wire Wire Line
	7250 1500 7250 2450
Connection ~ 7800 1500
Wire Wire Line
	9400 1050 9350 1050
Wire Wire Line
	9000 750  8900 750 
Wire Wire Line
	8900 800  8900 750 
Connection ~ 8900 750 
Wire Wire Line
	8900 750  8800 750 
Wire Wire Line
	9300 750  9350 750 
Wire Wire Line
	9350 750  9350 1050
Connection ~ 9350 1050
Wire Wire Line
	9350 1050 9300 1050
Wire Wire Line
	9000 1050 8900 1050
Wire Wire Line
	8900 1050 8900 1000
Wire Wire Line
	8650 2700 8900 2700
Wire Wire Line
	8650 2100 8650 2400
Connection ~ 8650 2100
Wire Wire Line
	8650 2100 8050 2100
Connection ~ 8900 2700
Connection ~ 8900 1050
$Comp
L Device:CP_Small C18
U 1 1 5CBBC805
P 9150 3150
F 0 "C18" H 8950 3100 50  0000 L CNN
F 1 "1uF" H 8950 3250 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 9150 3150 50  0001 C CNN
F 3 "~" H 9150 3150 50  0001 C CNN
	1    9150 3150
	-1   0    0    1   
$EndComp
$Comp
L Device:CP_Small C17
U 1 1 5CBBC91B
P 10100 2950
F 0 "C17" V 10000 2900 50  0000 L CNN
F 1 "1uF" V 10200 2900 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 10100 2950 50  0001 C CNN
F 3 "~" H 10100 2950 50  0001 C CNN
	1    10100 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	9150 3300 9150 3250
$Comp
L power:GND #PWR0131
U 1 1 5CCA39F7
P 9150 3300
F 0 "#PWR0131" H 9150 3050 50  0001 C CNN
F 1 "GND" H 9155 3127 50  0000 C CNN
F 2 "" H 9150 3300 50  0001 C CNN
F 3 "" H 9150 3300 50  0001 C CNN
	1    9150 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3050 9150 2950
Wire Wire Line
	9150 2950 9450 2950
Connection ~ 9450 2950
Wire Wire Line
	8900 1050 8900 2700
Wire Wire Line
	8550 5200 8350 5200
Wire Wire Line
	8350 5200 8350 3750
Wire Wire Line
	8350 3750 7000 3750
Wire Wire Line
	7000 3750 7000 750 
Wire Wire Line
	7000 750  8600 750 
Wire Wire Line
	6500 2950 6500 3050
Wire Wire Line
	10650 2950 10650 3050
$Comp
L Connector:Conn_01x10_Male J2
U 1 1 5D13F351
P 5400 1150
F 0 "J2" H 5500 1700 50  0000 C CNN
F 1 "Conn_01x10_Male" H 5506 1637 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 5400 1150 50  0001 C CNN
F 3 "~" H 5400 1150 50  0001 C CNN
	1    5400 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT P1
U 1 1 5D13F93A
P 9700 3750
F 0 "P1" V 9600 3850 50  0000 R CNN
F 1 "5K" V 9600 3700 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 9700 3750 50  0001 C CNN
F 3 "~" H 9700 3750 50  0001 C CNN
	1    9700 3750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_POT P3
U 1 1 5D13FA3C
P 5850 4450
F 0 "P3" H 5780 4404 50  0000 R CNN
F 1 "50K" H 5780 4495 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5850 4450 50  0001 C CNN
F 3 "~" H 5850 4450 50  0001 C CNN
	1    5850 4450
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_POT P4
U 1 1 5D21FF75
P 6350 4650
F 0 "P4" H 6280 4604 50  0000 R CNN
F 1 "50K" H 6280 4695 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6350 4650 50  0001 C CNN
F 3 "~" H 6350 4650 50  0001 C CNN
	1    6350 4650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:AudioJack2 J4
U 1 1 5D253C21
P 8000 5200
F 0 "J4" H 7820 5183 50  0000 R CNN
F 1 "OUT" H 7820 5274 50  0000 R CNN
F 2 "Theremin:mrPinHeader_1x02_P2.54mm_Vertical" H 8000 5200 50  0001 C CNN
F 3 "~" H 8000 5200 50  0001 C CNN
	1    8000 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	5600 950  6150 950 
Wire Wire Line
	6150 1150 5600 1150
Wire Wire Line
	5600 1250 6150 1250
Wire Wire Line
	5600 1550 6150 1550
Wire Wire Line
	6150 1650 5600 1650
Wire Wire Line
	1500 6950 1100 6950
Wire Wire Line
	1100 6950 1100 6750
Wire Wire Line
	1100 6550 1400 6550
Wire Wire Line
	1600 6550 1850 6550
Text Label 1300 6550 0    50   ~ 0
1
Text Label 1650 6550 0    50   ~ 0
2
Text Label 5700 750  0    50   ~ 0
1
Text Label 5700 850  0    50   ~ 0
2
Text Label 5700 950  0    50   ~ 0
3
Text Label 5700 1050 0    50   ~ 0
4
Text Label 5700 1150 0    50   ~ 0
5
Text Label 5700 1250 0    50   ~ 0
6
Text Label 5700 1350 0    50   ~ 0
7
Text Label 5700 1450 0    50   ~ 0
8
Text Label 5700 1550 0    50   ~ 0
9
Text Label 5700 1650 0    50   ~ 0
10
Text Label 3850 6550 0    50   ~ 0
8
Text Label 3600 6950 0    50   ~ 0
4
Text Label 3850 7350 0    50   ~ 0
3
Wire Wire Line
	6500 3250 6500 3300
Wire Wire Line
	6200 2950 6500 2950
Wire Wire Line
	10650 3250 10650 3300
Wire Wire Line
	10350 2950 10650 2950
Text Notes 1250 1150 0    50   ~ 0
VARIABLE PITCH OSCILLATOR
Text Notes 3350 1150 0    50   ~ 0
FIXED PITCH OSCILLATOR
Text Notes 5300 2050 0    50   ~ 0
PITCH TUNING
Text Notes 900  800  0    50   ~ 0
PITCH ANTENNA CIRCUIT
Text Notes 9500 750  0    50   ~ 0
VOLUME ANTENNA CIRCUIT
Text Notes 7350 1400 0    50   ~ 0
VOLUME OSCILLATOR
Text Notes 9550 2050 0    50   ~ 0
VOLUME TUNING
Text Notes 2500 6300 0    50   ~ 0
POWER SUPPLY
Text Notes 3700 4650 0    50   ~ 0
DETECTOR
Text Notes 5200 4050 0    50   ~ 0
VOLTAGE CONTROLLED AMPLIFIER
Text Notes 9100 4050 0    50   ~ 0
VCA PROCESSOR
$Comp
L Switch:SW_SPST SW1
U 1 1 5D4CFD96
P 5950 750
F 0 "SW1" H 5950 893 50  0000 C CNN
F 1 "SW_SPST" H 5950 894 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5950 750 50  0001 C CNN
F 3 "~" H 5950 750 50  0001 C CNN
	1    5950 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 750  5750 750 
Wire Wire Line
	6150 750  6150 850 
Wire Wire Line
	5600 850  6150 850 
Text Notes 9800 3600 0    50   ~ 0
VOLUME TUNING
Text Notes 5650 3600 0    50   ~ 0
PITCH TUNING
Text Notes 5900 4350 0    50   ~ 0
WAVEFORM
Text Notes 6400 4550 0    50   ~ 0
BRIGHTNESS
Text Label 5550 3550 0    50   ~ 0
5
Text Label 9700 3550 0    50   ~ 0
6
Text Label 5250 4450 0    50   ~ 0
7
Text Label 5600 4650 0    50   ~ 0
9
Text Label 7650 5200 0    50   ~ 0
10
$Comp
L power:-12V #PWR0132
U 1 1 5D542B13
P 6500 6100
F 0 "#PWR0132" H 6500 6200 50  0001 C CNN
F 1 "-12V" H 6600 6250 50  0000 C CNN
F 2 "" H 6500 6100 50  0001 C CNN
F 3 "" H 6500 6100 50  0001 C CNN
	1    6500 6100
	1    0    0    1   
$EndComp
Wire Wire Line
	6500 5600 6500 6100
Wire Wire Line
	4500 4050 10500 4050
Wire Wire Line
	9800 6000 10850 6000
Wire Wire Line
	9900 5900 10850 5900
Wire Wire Line
	7800 5200 7500 5200
Connection ~ 7500 5200
Wire Wire Line
	7500 5600 7800 5600
Wire Wire Line
	7800 5600 7800 5300
Connection ~ 7500 5600
Wire Wire Line
	9700 3250 9700 3600
Wire Wire Line
	5550 3250 5550 3600
$Comp
L power:-12V #PWR0133
U 1 1 5D9D7940
P 5800 3750
F 0 "#PWR0133" H 5800 3850 50  0001 C CNN
F 1 "-12V" V 5800 4000 50  0000 C CNN
F 2 "" H 5800 3750 50  0001 C CNN
F 3 "" H 5800 3750 50  0001 C CNN
	1    5800 3750
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0134
U 1 1 5D9D7F4D
P 5300 3750
F 0 "#PWR0134" H 5300 3500 50  0001 C CNN
F 1 "GND" V 5300 3600 50  0000 R CNN
F 2 "" H 5300 3750 50  0001 C CNN
F 3 "" H 5300 3750 50  0001 C CNN
	1    5300 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	5300 3750 5400 3750
Wire Wire Line
	5700 3750 5800 3750
$Comp
L power:-12V #PWR0135
U 1 1 5DA1B19F
P 9950 3750
F 0 "#PWR0135" H 9950 3850 50  0001 C CNN
F 1 "-12V" V 9950 4000 50  0000 C CNN
F 2 "" H 9950 3750 50  0001 C CNN
F 3 "" H 9950 3750 50  0001 C CNN
	1    9950 3750
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0136
U 1 1 5DA1B861
P 9450 3750
F 0 "#PWR0136" H 9450 3500 50  0001 C CNN
F 1 "GND" V 9450 3600 50  0000 R CNN
F 2 "" H 9450 3750 50  0001 C CNN
F 3 "" H 9450 3750 50  0001 C CNN
	1    9450 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 3750 9550 3750
Wire Wire Line
	9850 3750 9950 3750
$Comp
L power:-12V #PWR0137
U 1 1 5DA7F751
P 5850 4300
F 0 "#PWR0137" H 5850 4400 50  0001 C CNN
F 1 "-12V" H 5865 4473 50  0000 C CNN
F 2 "" H 5850 4300 50  0001 C CNN
F 3 "" H 5850 4300 50  0001 C CNN
	1    5850 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5200 4450 5700 4450
Wire Wire Line
	5500 4650 6200 4650
$Comp
L power:GND #PWR0138
U 1 1 5DB49598
P 6350 4900
F 0 "#PWR0138" H 6350 4650 50  0001 C CNN
F 1 "GND" H 6450 4750 50  0001 R CNN
F 2 "" H 6350 4900 50  0001 C CNN
F 3 "" H 6350 4900 50  0001 C CNN
	1    6350 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0139
U 1 1 5DB4999A
P 6350 4300
F 0 "#PWR0139" H 6350 4150 50  0001 C CNN
F 1 "+12V" H 6365 4473 50  0000 C CNN
F 2 "" H 6350 4300 50  0001 C CNN
F 3 "" H 6350 4300 50  0001 C CNN
	1    6350 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4300 6350 4500
Wire Wire Line
	6350 4900 6350 4850
Wire Wire Line
	6350 4850 5850 4850
Wire Wire Line
	5850 4600 5850 4850
Connection ~ 6350 4850
Wire Wire Line
	6350 4850 6350 4800
Wire Wire Line
	5600 1050 6150 1050
Wire Wire Line
	5600 1350 6150 1350
Wire Wire Line
	5600 1450 6150 1450
Text Label 5850 1050 0    50   ~ 0
GND
Text Label 5850 950  0    50   ~ 0
-12V
Text Label 5850 1450 0    50   ~ 0
+12V
Wire Wire Line
	10850 5400 10550 5400
Text Label 10550 5400 0    50   ~ 0
AUX
Wire Wire Line
	7500 5600 7500 5700
Wire Wire Line
	7200 5600 7500 5600
NoConn ~ 10550 5400
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E286DB0
P 2750 6550
F 0 "#FLG0101" H 2750 6625 50  0001 C CNN
F 1 "PWR_FLAG" H 2750 6723 50  0000 C CNN
F 2 "" H 2750 6550 50  0001 C CNN
F 3 "~" H 2750 6550 50  0001 C CNN
	1    2750 6550
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E28781B
P 2750 7350
F 0 "#FLG0102" H 2750 7425 50  0001 C CNN
F 1 "PWR_FLAG" H 2750 7523 50  0000 C CNN
F 2 "" H 2750 7350 50  0001 C CNN
F 3 "~" H 2750 7350 50  0001 C CNN
	1    2750 7350
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5E2A7244
P 1100 6950
F 0 "#FLG0103" H 1100 7025 50  0001 C CNN
F 1 "PWR_FLAG" H 1100 7123 50  0000 C CNN
F 2 "" H 1100 6950 50  0001 C CNN
F 3 "~" H 1100 6950 50  0001 C CNN
	1    1100 6950
	-1   0    0    1   
$EndComp
Connection ~ 1100 6950
$Comp
L Device:R_POT P2
U 1 1 5D13F701
P 5550 3750
F 0 "P2" V 5450 3850 50  0000 R CNN
F 1 "5K" V 5450 3700 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5550 3750 50  0001 C CNN
F 3 "~" H 5550 3750 50  0001 C CNN
	1    5550 3750
	0    -1   -1   0   
$EndComp
$Comp
L Amplifier_Operational:TL081 U4
U 1 1 5CB203CB
P 5500 7200
F 0 "U4" H 5600 7300 50  0000 L CNN
F 1 "TL081" H 5500 7400 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 5550 7250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl081.pdf" H 5650 7350 50  0001 C CNN
	1    5500 7200
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R43
U 1 1 5CB215C1
P 6100 7150
F 0 "R43" V 5900 7150 50  0000 C CNN
F 1 "330" V 6000 7150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6100 7150 50  0001 C CNN
F 3 "~" H 6100 7150 50  0001 C CNN
	1    6100 7150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R44
U 1 1 5CB22035
P 6100 7250
F 0 "R44" V 6200 7250 50  0000 C CNN
F 1 "330" V 6300 7250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6100 7250 50  0001 C CNN
F 3 "~" H 6100 7250 50  0001 C CNN
	1    6100 7250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R42
U 1 1 5CB223E1
P 5700 6850
F 0 "R42" V 5800 6850 50  0000 C CNN
F 1 "100K" V 5900 6850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5700 6850 50  0001 C CNN
F 3 "~" H 5700 6850 50  0001 C CNN
	1    5700 6850
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C31
U 1 1 5CB22A42
P 5700 6650
F 0 "C31" V 5600 6650 50  0000 C CNN
F 1 "100pF" V 5800 6650 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 5700 6650 50  0001 C CNN
F 3 "~" H 5700 6650 50  0001 C CNN
	1    5700 6650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R41
U 1 1 5CB232DE
P 4900 7100
F 0 "R41" V 4800 7100 50  0000 C CNN
F 1 "10K" V 5000 7100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4900 7100 50  0001 C CNN
F 3 "~" H 4900 7100 50  0001 C CNN
	1    4900 7100
	0    1    1    0   
$EndComp
$Comp
L Connector:AudioJack3 J6
U 1 1 5CB42B31
P 6500 7250
F 0 "J6" H 6220 7183 50  0000 R CNN
F 1 "Phones" H 6650 7500 50  0000 R CNN
F 2 "Theremin:mrPinHeader_1x04_P2.54mm_Vertical" H 6500 7250 50  0001 C CNN
F 3 "~" H 6500 7250 50  0001 C CNN
	1    6500 7250
	-1   0    0    1   
$EndComp
$Comp
L power:-12V #PWR0140
U 1 1 5CBA6904
P 5400 6550
F 0 "#PWR0140" H 5400 6650 50  0001 C CNN
F 1 "-12V" H 5300 6700 50  0000 C CNN
F 2 "" H 5400 6550 50  0001 C CNN
F 3 "" H 5400 6550 50  0001 C CNN
	1    5400 6550
	-1   0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0141
U 1 1 5CBA6E9F
P 5400 7600
F 0 "#PWR0141" H 5400 7450 50  0001 C CNN
F 1 "+12V" H 5250 7700 50  0000 C CNN
F 2 "" H 5400 7600 50  0001 C CNN
F 3 "" H 5400 7600 50  0001 C CNN
	1    5400 7600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 7100 5100 7100
Wire Wire Line
	5100 7100 5100 6850
Wire Wire Line
	5100 6850 5600 6850
Connection ~ 5100 7100
Wire Wire Line
	5100 7100 5200 7100
Wire Wire Line
	6000 7150 5900 7150
Wire Wire Line
	5900 7150 5900 7200
Wire Wire Line
	5900 7250 6000 7250
Wire Wire Line
	5800 7200 5900 7200
Connection ~ 5900 7200
Wire Wire Line
	5900 7200 5900 7250
Wire Wire Line
	5900 7150 5900 6850
Wire Wire Line
	5900 6850 5800 6850
Connection ~ 5900 7150
Wire Wire Line
	5400 6900 5400 6550
Wire Wire Line
	5400 7500 5400 7600
$Comp
L power:GND #PWR05
U 1 1 5CC89803
P 5100 7450
F 0 "#PWR05" H 5100 7200 50  0001 C CNN
F 1 "GND" H 5105 7277 50  0000 C CNN
F 2 "" H 5100 7450 50  0001 C CNN
F 3 "" H 5100 7450 50  0001 C CNN
	1    5100 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 7300 5100 7300
Wire Wire Line
	5100 7300 5100 7450
Wire Wire Line
	5600 6650 5100 6650
Wire Wire Line
	5100 6650 5100 6850
Connection ~ 5100 6850
Wire Wire Line
	5800 6650 5900 6650
Wire Wire Line
	5900 6650 5900 6850
Connection ~ 5900 6850
$Comp
L power:GND #PWR06
U 1 1 5CD50AEB
P 6200 7450
F 0 "#PWR06" H 6200 7200 50  0001 C CNN
F 1 "GND" H 6205 7277 50  0000 C CNN
F 2 "" H 6200 7450 50  0001 C CNN
F 3 "" H 6200 7450 50  0001 C CNN
	1    6200 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 7450 6200 7350
Wire Wire Line
	6200 7350 6300 7350
Wire Wire Line
	6200 7250 6300 7250
Wire Wire Line
	6200 7150 6300 7150
$Comp
L Connector:Conn_01x01_Male J5
U 1 1 5CDDDD79
P 4400 7100
F 0 "J5" H 4508 7189 50  0000 C CNN
F 1 "Conn_01x01_Male" H 4508 7190 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4400 7100 50  0001 C CNN
F 3 "~" H 4400 7100 50  0001 C CNN
	1    4400 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 7100 4800 7100
Text Label 4650 7100 0    50   ~ 0
10
$Comp
L Amplifier_Operational:CA3080 U5
U 1 1 5CE25D30
P 2400 5400
F 0 "U5" H 2550 5300 50  0000 C CNN
F 1 "CA3080" H 2500 5200 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 2400 5400 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/ca30/ca3080-a.pdf" H 2400 5500 50  0001 C CNN
	1    2400 5400
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR02
U 1 1 5CE4AB75
P 2300 4950
F 0 "#PWR02" H 2300 4800 50  0001 C CNN
F 1 "+12V" H 2315 5123 50  0000 C CNN
F 2 "" H 2300 4950 50  0001 C CNN
F 3 "" H 2300 4950 50  0001 C CNN
	1    2300 4950
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR03
U 1 1 5CE4B2F5
P 2300 6000
F 0 "#PWR03" H 2300 6100 50  0001 C CNN
F 1 "-12V" H 2400 6150 50  0000 C CNN
F 2 "" H 2300 6000 50  0001 C CNN
F 3 "" H 2300 6000 50  0001 C CNN
	1    2300 6000
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R48
U 1 1 5CE4BC16
P 2800 5650
F 0 "R48" H 2850 5750 50  0000 L CNN
F 1 "10K" H 2850 5550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2800 5650 50  0001 C CNN
F 3 "~" H 2800 5650 50  0001 C CNN
	1    2800 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C32
U 1 1 5CE4C136
P 3000 5650
F 0 "C32" H 3100 5650 50  0000 L CNN
F 1 "1nF" H 3050 5550 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3000 5650 50  0001 C CNN
F 3 "~" H 3000 5650 50  0001 C CNN
	1    3000 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R46
U 1 1 5CE4C773
P 2000 5650
F 0 "R46" H 2050 5650 50  0000 L CNN
F 1 "1K" H 2050 5550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2000 5650 50  0001 C CNN
F 3 "~" H 2000 5650 50  0001 C CNN
	1    2000 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R45
U 1 1 5CE4CF1E
P 1800 5650
F 0 "R45" H 1700 5700 50  0000 C CNN
F 1 "47K" H 1700 5600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 1800 5650 50  0001 C CNN
F 3 "~" H 1800 5650 50  0001 C CNN
	1    1800 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R47
U 1 1 5CE4DE60
P 2600 5100
F 0 "R47" V 2500 5100 50  0000 C CNN
F 1 "220K" V 2700 5100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2600 5100 50  0001 C CNN
F 3 "~" H 2600 5100 50  0001 C CNN
	1    2600 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 5550 2000 5300
Connection ~ 2000 5300
Wire Wire Line
	2000 5300 2100 5300
$Comp
L power:GND #PWR01
U 1 1 5CE959CB
P 2000 5950
F 0 "#PWR01" H 2000 5700 50  0001 C CNN
F 1 "GND" H 2005 5777 50  0000 C CNN
F 2 "" H 2000 5950 50  0001 C CNN
F 3 "" H 2000 5950 50  0001 C CNN
	1    2000 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 4950 2300 5100
Wire Wire Line
	2300 5700 2300 5800
$Comp
L power:+12V #PWR04
U 1 1 5CF4B6C0
P 2800 4950
F 0 "#PWR04" H 2800 4800 50  0001 C CNN
F 1 "+12V" H 2815 5123 50  0000 C CNN
F 2 "" H 2800 4950 50  0001 C CNN
F 3 "" H 2800 4950 50  0001 C CNN
	1    2800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5100 2500 5100
Wire Wire Line
	2700 5100 2800 5100
Wire Wire Line
	2800 5100 2800 4950
$Comp
L Connector:AudioJack2 J8
U 1 1 5CF9B081
P 3400 5400
F 0 "J8" H 3220 5383 50  0000 R CNN
F 1 "OUTT" H 3220 5474 50  0000 R CNN
F 2 "Theremin:mrPinHeader_1x02_P2.54mm_Vertical" H 3400 5400 50  0001 C CNN
F 3 "~" H 3400 5400 50  0001 C CNN
	1    3400 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	2800 5550 2800 5400
Wire Wire Line
	2700 5400 2800 5400
Connection ~ 2800 5400
Wire Wire Line
	2800 5400 3000 5400
Wire Wire Line
	3000 5550 3000 5400
Connection ~ 3000 5400
Wire Wire Line
	3000 5400 3200 5400
Wire Wire Line
	2000 5750 2000 5900
Wire Wire Line
	2000 5900 2800 5900
Wire Wire Line
	2800 5900 2800 5750
Connection ~ 2000 5900
Wire Wire Line
	2000 5900 2000 5950
Wire Wire Line
	2800 5900 3000 5900
Wire Wire Line
	3000 5900 3000 5750
Connection ~ 2800 5900
$Comp
L Connector:Conn_01x01_Male J7
U 1 1 5D085576
P 1150 5500
F 0 "J7" H 1250 5450 50  0000 C CNN
F 1 "Conn_01x01_Male" H 1258 5590 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1150 5500 50  0001 C CNN
F 3 "~" H 1150 5500 50  0001 C CNN
	1    1150 5500
	1    0    0    -1  
$EndComp
Text Label 1450 5500 0    50   ~ 0
AUDIO
Wire Wire Line
	1800 5750 1800 5800
Wire Wire Line
	1800 5800 2300 5800
Connection ~ 2300 5800
Wire Wire Line
	2300 5800 2300 6000
Wire Wire Line
	1800 5550 1800 5300
Wire Wire Line
	1800 5300 2000 5300
Wire Wire Line
	3200 5500 3200 5900
Wire Wire Line
	3200 5900 3000 5900
Connection ~ 3000 5900
Text Notes 1450 5050 0    50   ~ 0
TUNER AMPLIFIER
Text Notes 4350 6550 0    50   ~ 0
HEADPHONES AMPLIFIER
NoConn ~ 5500 6900
NoConn ~ 5600 6900
Wire Wire Line
	1350 5500 2100 5500
Wire Wire Line
	4500 3050 4500 3300
Wire Wire Line
	3300 3000 3300 3300
Text Notes 900  650  0    50   ~ 0
L1 - L4=J.W. Miller #6306\n3-section RF choke
Text Notes 2300 1450 0    50   ~ 0
L5, L6= 100uH Toko RWRS-T1015Z\nhi-Q variable inductor
Text Notes 9000 1400 0    50   ~ 0
L7, L8=J.W. Miller #6302\n3-section RF choke
Text Notes 10100 1400 0    50   ~ 0
L9, L10=J.W. Miller #6304\n3-section RF choke
Text Notes 7250 1200 0    50   ~ 0
L11= 68uH Toko 154ANS-T1019Z\nhi-Q variable inductor
$EndSCHEMATC
